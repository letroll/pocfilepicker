package fr.letroll.pocfilepicker

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.content_fragment.*
import java.io.BufferedReader
import java.io.InputStreamReader


class TotoFragment : Fragment() {

    private var mLine: String? = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.content_fragment,container,false)
        return view!!
    }

    fun readFile(mainActivity: MainActivity, pathHolder: Uri) {
        val inputStream = mainActivity.contentResolver.openInputStream(pathHolder)
        inputStream?.let {
            val reader = BufferedReader(InputStreamReader(inputStream))
            updateLine(reader)
            do {
                tv_result.append(mLine)
                tv_result.append("\n")
            } while (updateLine(reader) != null)
        }
    }

    private fun updateLine(r: BufferedReader): String? {
        mLine = r.readLine()
        return mLine
    }
}